from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe, Step, Ingredient
from django.contrib.auth.decorators import login_required
from recipes.forms import RecipeForm


# Create your views here.


def all_recipes(request):
    recipes = Recipe.objects.all
    context = {"recipes": recipes}
    return render(request, "recipes/all_recipes.html", context)


def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {"recipe": recipe}
    return render(request, "recipes/detail.html", context)


@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("all_recipes")
    else:
        form = RecipeForm()
    context = {"form": form}
    return render(request, "recipes/create.html", context)


@login_required
def edit_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id)
    else:
        form = RecipeForm(instance=recipe)
        context = {"form": form}
    return render(request, "recipes/edit.html", context)


@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {"recipe_list": recipes}
    return render(request, "recipes/all_recipes.html", context)
