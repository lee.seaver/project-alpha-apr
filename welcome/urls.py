from django.urls import path
from welcome.views import welcome
from . import views

urlpatterns = [path("", views.welcome, name="welcome")]
