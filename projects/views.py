from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.


@login_required
def list_projects(request):
    project_instances = Project.objects.filter(owner=request.user)
    context = {"project_instances": project_instances}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project_instance = Project.objects.get(id=id)
    context = {"project_instance": project_instance}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect(show_project, project.id)
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)


@login_required
def delete_project(request, id):
    project_instance = Project.objects.get(id=id)
    if request.method == "POST":
        project_instance.delete()
        return redirect("list_projects")
    return render(request, "projects/delete.html")


@login_required
def edit_project(request, id):
    project = Project.objects.get(id=id)
    if request.method == "POST":
        form = ProjectForm(request.POST, instance=project)
        if form.is_valid():
            project = form.save()
            return redirect(show_project, id)
    else:
        form = ProjectForm(instance=project)
    context = {"form": form}
    return render(request, "projects/edit.html", context)
