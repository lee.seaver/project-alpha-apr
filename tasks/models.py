from django.db import models
from projects.models import Project
from django.conf import settings
import datetime

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateField(default=datetime.datetime.now, null=False)
    due_date = models.DateField(auto_now_add=False, null=False)
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )

    class Meta:
        ordering = ["is_completed"]
